angular.module("photoWorld").directive("header", function () {
	return {
		restrict: "A", 		
		templateUrl: "../templates/header.html",
		controller: ["$rootScope", "$scope", "authenticationService", "$state", function ($rootScope,$scope, authenticationService, $state) {

			$scope.signOut = function() {
				
				authenticationService.$signOut().then(function(some){
					$state.go("userLogin");
				});		
				// $state.go("userLogin");		
			  };
		}]
	};
});