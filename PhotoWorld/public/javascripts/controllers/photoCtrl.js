(function(){
	angular.module("photoWorld")
		.controller("photoCtrl", ["$rootScope", "$scope", "$location", "photoService", "$stateParams", function($rootScope, $scope, $location, photoService, $stateParams){
			$rootScope.loaded=true;
			$scope.openModal= function () { 
				$(".modal").modal();
				$("#newPhotoModal").modal("open");
			};
			$scope.photos=[];
			$scope.newPhoto={};

			$scope.photos=photoService.getThePhotos();

		
			$scope.addNewPhoto = function(newPhoto){
				newPhoto.albumId=parseInt($stateParams.id);
				newPhoto.id= $scope.photos.length+1;
				photoService.postToFirebasePhotos(newPhoto);
				
				$scope.existingPhoto={};
				$scope.addPhoto= false;
			};

    
			$scope.editingThisPhoto = function(existingPhoto){
				$scope.editingPhoto=true;
				$scope.existingPhoto=existingPhoto;
			};
    
			$scope.editPhoto = function(newPhoto){
				photoService.updateFirebasePhoto(newPhoto);
        
				$scope.newPhoto={};
				$scope.editingPhoto= false;
			};

			$scope.deletePhoto = function(photo) {
				photoService.deleteFirebasePhoto(photo);
			};


		}]);
})();