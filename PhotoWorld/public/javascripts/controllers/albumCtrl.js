(function(){
	angular.module("photoWorld")
		.controller("albumCtrl", ["$rootScope", "$scope", "$location", "photoService", "$stateParams", function($rootScope, $scope, $location, photoService, $stateParams){

			$scope.openModal= function () { 
				$(".modal").modal();
				$("#newAlbumModal").modal("open");
			};

			$scope.albums=[];
			$scope.newAlbum={}; 


			photoService.getFirebaseAlbums().$loaded().then(albums=>{
				$scope.albums=albums;
				$rootScope.loaded=true;
			});
			

			photoService.getFirebaseOwners().$loaded().then(owners=>{
				$scope.owners=owners;
			});

			$scope.goToPhotos = function(albumId) {
				photoService.getFirebasePhotos(albumId);
				$rootScope.loaded=false;
				$location.path("/photos/" + albumId);	
				
			};
        

			//Needs work
			$scope.addNewAlbum = function(newAlbum){
				let owner = {
					address:{
						city: "New York",
						geo: {
							lat: "43",
							lng: "03"
						},
						street: "123 Main Street",
						suite: "3",
						zipcode: "30130"
					},
					email:"example@email.com",
					id: $scope.owners.length+1,
					name: newAlbum.owner,
					username: "theog"};
				photoService.postToFirebaseOwners(owner);

				newAlbum.id= $scope.albums.length+1;
				newAlbum.userId= $scope.owners.length+1;
				photoService.postToFirebaseAlbums(newAlbum);
				
				$scope.existingAlbum={};
				$scope.addAlbum= false;
			};

    
			$scope.editingThisAlbum = function(existingAlbum){
				$scope.editingAlbum=true;
				$scope.existingAlbum=existingAlbum;
			};
    
			$scope.editAlbum = function(newAlbum){
				photoService.updateFirebaseAlbum(newAlbum);
        
				$scope.newAlbum={};
				$scope.editingAlbum= false;
			};

			$scope.deleteAlbum = function(album) {
				photoService.deleteFirebaseAlbum(album);
			};

		}]);
})();