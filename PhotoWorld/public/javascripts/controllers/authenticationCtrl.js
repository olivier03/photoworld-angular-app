(function(){
	angular.module("photoWorld").controller("authenticationCtrl", ["$rootScope", "$scope", "authenticationService", "$state", ($rootScope, $scope, authenticationService, $state) => {
		
		authenticationService.$onAuthStateChanged(function(auth) {
			if (auth) {
				Materialize.toast("Auth changed to " + auth.email, 3000, "rounded");
			} else {
				Materialize.toast("Signed Out", 3000, "rounded");
			}
		});

		$scope.login = function (email, password){
			authenticationService.$signInWithEmailAndPassword(email, password).then(function (auth){
				$state.go("albums");
			}, function (error){
				Materialize.toast(error.message, 3000, "rounded");
			});
		};

		$scope.signUp = function (email, password){
			authenticationService.$createUserWithEmailAndPassword(email, password).then(function (user){
				Materialize.toast("User " + user.uid + " created successfully!", 3000, "rounded");
			  $state.go("albums");
			}, function (error){
				Materialize.toast(error.message, 3000, "rounded");
			});
		  };

	}]);
})();