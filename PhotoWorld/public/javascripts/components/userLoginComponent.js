(function(){
	angular.module("photoWorld").component("userLogin", {
		templateUrl: "templates/userLogin.html",
		controller: "authenticationCtrl"
	});
})();