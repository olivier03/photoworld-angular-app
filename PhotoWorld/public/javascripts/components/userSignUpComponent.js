(function(){
	angular.module("photoWorld").component("userSignUp", {
		templateUrl: "templates/userSignUp.html",
		controller: "authenticationCtrl"
	});
})();