(function() {
	angular.module("photoWorld").component("photos", {
		bindings: {photos: "<"},
		templateUrl: "templates/photos.html",
		controller: "photoCtrl"
	});
})();