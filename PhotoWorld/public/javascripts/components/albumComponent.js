(function(){
	angular.module("photoWorld").component("albums", {
		bindings: { owners: "<" },
		templateUrl: "templates/albums.html",
		controller: "albumCtrl"
	});
})();