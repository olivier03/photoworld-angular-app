(function(){
	angular.module("photoWorld")
		.config(["$stateProvider", "$urlRouterProvider", function($stateProvider, $urlRouterProvider){
			$stateProvider.state("userLogin", {
				url : "/userLogin",
				component : "userLogin",
				useAsDefault : true,
				resolve : {
					requireNoAuth: ["$state", "authenticationService", function($state, authenticationService){
						return authenticationService.$requireSignIn().then(function(auth){
						  $state.go("albums");
						}, function(error){
						  return;
						});
					  }]
				}
			})
				.state("userSignUp", {
					url : "/userSignUp",
					component : "userSignUp",
					resolve : {
						requireNoAuth: ["$state", "authenticationService", function($state, authenticationService){
							return authenticationService.$requireSignIn().then(function(auth){
							  $state.go("albums");
							}, function(error){
							  return;
							});
						  }]
					}
				})
				.state("albums", {
					url : "/albums",
					component: "albums",
					resolve: {
						requireAuth: ["$state", "authenticationService", function($state, authenticationService){
							return authenticationService.$requireSignIn().then(function(auth){
								Materialize.toast("Welcome " + auth.email, 3000, "rounded");
							  	$state.go("albums");
							}, function(error){
								Materialize.toast(error, 3000, "rounded");
								$state.go("userLogin");
							});
						  }],
						owners: ["photoService", function(photoService) {
						// load the view, when the data is available
						// this is where $loaded() belongs 					  
							return [{userId:1, name:"Olivier"}];
						}], 
					}
				})
				.state("photos", {
					url : "/photos/:id",
					component: "photos",
					resolve: {
						requireAuth: ["$state", "authenticationService", function($state, authenticationService){
							return authenticationService.$requireSignIn().then(function(auth){
								Materialize.toast("Welcome " + auth.email, 3000, "rounded");
							  	$state.go("/photos/:id");
							}, function(error){
								Materialize.toast(error, 3000, "rounded");
								$state.go("userLogin");
							});
						  }],
						photos: ["photoService", function(photoService) {
							// load the view, when the data is available
							// this is where $loaded() belongs 					  
							//return photoService.getFirebasePhotos(3).$loaded();
						}], 
					}
				});
			$urlRouterProvider.otherwise("userLogin");
		}]).run(function($state, $rootScope){
			$rootScope.$state = $state;
		});
})();

