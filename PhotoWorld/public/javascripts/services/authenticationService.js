(function(){
	angular.module("photoWorld")
		.factory("authenticationService", ["$firebaseAuth", function($firebaseAuth){ 

			var config = {
				apiKey: "AIzaSyAHyFDOHMDTXhFJGxz_iZmblyUH7ZLkntI",
				authDomain: "photoworld-f6f9d.firebaseapp.com",
				databaseURL: "https://photoworld-f6f9d.firebaseio.com",
				projectId: "photoworld-f6f9d",
				storageBucket: "photoworld-f6f9d.appspot.com",
				messagingSenderId: "198700145911"
			};
			firebase.initializeApp(config);

			var auth = $firebaseAuth();            
			return auth;
			
		}]);
})();