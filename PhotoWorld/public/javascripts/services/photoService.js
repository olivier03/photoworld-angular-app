(function(){
	angular.module("photoWorld")
		.factory("photoService", ["$q", "$http", "$firebaseObject", "$firebaseArray", function($q, $http, $firebaseObject, $firebaseArray){
			//data
        
			var albums;
			var photos;
			var owners;
        
	   

			var ownersRef =firebase.database().ref("owners");
			var photosRef =firebase.database().ref("photos");
			var albumsRef =firebase.database().ref("albums");
			var allPhotos= $firebaseArray(photosRef);
            
			function getFirebaseAlbums(){
				return albums = $firebaseArray(albumsRef);
			}
            
			function getFirebasePhotos(albumId){				
				return photos = $firebaseArray(photosRef.orderByChild("albumId").equalTo(albumId));
			}

			function getThePhotos(){
				return photos;
			}

			function getFirebaseOwners(){
				return owners=$firebaseArray(ownersRef);
			}
        
			function postToFirebaseAlbums(album){
				$firebaseArray(albumsRef).$add(album).then(data => {
					console.log("Album added to Firebase");
				}, error => {
					console.log("There was an error", error);
				});
			}

			function postToFirebasePhotos(photo){
				$firebaseArray(photosRef).$add(photo).then(data => {
					console.log("Photo added to Firebase");
				}, error => {
					console.log("There was an error", error);
				});
			}

			function postToFirebaseOwners(owner){
				$firebaseArray(ownersRef).$add(owner).then(data => {
					console.log("Owner added to Firebase");
				}, error => {
					console.log("There was an error", error);
				});
			}

			function updateFirebaseAlbum(newAlbum){
				let album=albums.$getRecord(newAlbum.$id);
				album.title=newAlbum.title;
				albums.$save(album).then(data => {
					console.log(data);
				}, error => {
					console.log("There was an error", error);
				});
				
			}

			function updateFirebasePhoto(newPhoto){
				let photo = allPhotos.$getRecord(newPhoto.$id);
				photo.title=newPhoto.title;
				photo.url=newPhoto.url;
				photo.thumbnailUrl=newPhoto.thumbnailUrl;
				allPhotos.$save(photo).then(data => {
					console.log(data);
				}, error => {
					console.log("There was an error", error);
				});
				
			}

			function deleteFirebaseAlbum(album){
				let indexToBeDeleted=albums.$indexFor(album.$id);
				albums.$remove(indexToBeDeleted);
				
			}

			function deleteFirebasePhoto(photo){
				let indexToBeDeleted=photos.$indexFor(photo.$id);
				photos.$remove(indexToBeDeleted);
			}


			function getAlbums(){
            
				return $http.get("http://jsonplaceholder.typicode.com/albums").then(function(response){
					albums= response.data;
					return $http.get("http://jsonplaceholder.typicode.com/photos");
				}, function(error){
					console.log("Error: " + error.status + " : " + error.statusText);
				}).then(function(response){
					photos=response.data;
					albums.forEach(function(album){
						//console.log("breaking", photo);
						//break;
						countPhotos=0;
						//i = 0;
                    
						photos.forEach(function(photo){
							if (album.id === photo.albumId){
								countPhotos++;
								album.countPhotos=countPhotos;
							}
						});
					});
					return albums;
				}, function(error){
					console.log("Error: " + error.status + " : " + error.statusText);
				});
			}


			return {
				getFirebaseAlbums : getFirebaseAlbums,
				getFirebasePhotos : getFirebasePhotos,
				getThePhotos : getThePhotos,
				getFirebaseOwners : getFirebaseOwners,
				postToFirebaseAlbums : postToFirebaseAlbums,
				postToFirebasePhotos : postToFirebasePhotos,
				postToFirebaseOwners : postToFirebaseOwners,
				updateFirebaseAlbum : updateFirebaseAlbum,
				updateFirebasePhoto : updateFirebasePhoto,
				deleteFirebaseAlbum : deleteFirebaseAlbum,
				deleteFirebasePhoto : deleteFirebasePhoto,
				getAlbums : getAlbums
			};
		}]);
})();